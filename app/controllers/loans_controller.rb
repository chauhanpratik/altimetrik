class LoansController < ApplicationController

  def index
    
    @loan_records = []
    @investor_details = {}
    (1..15).each do |n|

      # Assign Loan details
      category = params["loan_category_#{n}"]
      risk_band = params["loan_risk_band_#{n}"]
      loan_amount = params["loan_amount_#{n}"].to_i rescue nil
      loan_amount = 0 if loan_amount.blank?
      if loan_amount > 0
        @loan_records << {loan_id: n,category: category,risk_band: risk_band,amount: loan_amount,get_loan: false}
      end

      # Assign Investor details
      investor_name = params["investor_name_#{n}"]
      investor_amount = params["investor_amount_#{n}"].to_i rescue nil
      invest_percentage = params["invest_percentage_#{n}"].to_i rescue nil
      investor_category = params["investor_category_#{n}"]
      investor_risk = params["investor_risk_#{n}"]
      if investor_name.present?
        investor_category = investor_category.split(",") if investor_category.present?
        investor_risk = investor_risk.split(",") if investor_risk.present?
        investor_category = [] if investor_category.blank?
        investor_risk = [] if investor_risk.blank?
        @investor_details[investor_name] = {balance: investor_amount,remaining_balance: investor_amount,invest_loan_category: investor_category,risk_band: investor_risk,invest_percentage: invest_percentage}
      end
    end
    


    @loan_with_investor = []
    # Check Loan with Investor

    @loan_records.each do |loan|
      #Assu
      is_get_loan = false 
      loan_id = loan[:loan_id]
      loan_category = loan[:category]
      loan_risk_band = loan[:risk_band]
      loan_amount = loan[:amount]
      next if loan_amount < 1
      @investor_details.each do |investor_key,investor_value|
        break if is_get_loan
        if investor_value[:invest_loan_category].include?(loan_category)
          if investor_value[:risk_band].include?(loan_risk_band)
            investor_remaining_balance = investor_value[:remaining_balance]

            if (1..investor_remaining_balance).include?(loan_amount)
              investor_new_remaining_balance = (investor_remaining_balance - loan_amount).abs rescue 0
              investor_value[:remaining_balance] = investor_new_remaining_balance
              is_get_loan = true
              @loan_with_investor << {loan_id: loan_id,loan_category: loan_category,risk_band: loan_risk_band,loan_amount: loan_amount,investor_name: investor_key,investor_balance: investor_value[:balance],investor_remaining_balance: investor_new_remaining_balance}
            end

          end
        end
      end
    end
    

    @loan_with_investor_details = {"Total Loan" => @loan_records.count,"Approved Loan" => @loan_with_investor.count,"Approved Loan Details" => @loan_with_investor}


  end

  def loan

    loan_records = []
    loan_categories = ["Property","Retail","Medical"]
    risk_band_list = {"1" => "A+","2" => "A","3" => "B","4" => "C","5" => "D","6" => "E"}
    dummy_loan_amount_array = [5000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000, 55000, 60000, 65000, 70000, 75000, 80000, 85000, 90000, 95000, 100000]

    (1..20).each do |n|
    category = loan_categories.sample
    risk_band = risk_band_list.values.sample
    amount = dummy_loan_amount_array.sample
    loan_records << {loan_id: n,category: category,risk_band: risk_band,amount: amount,get_loan: false}
    end


    investor_details = {}

    investor_names = ["Bob","Susan","George","Helen","Jamie"]
    dummy_investor_amount_array = [45000, 50000, 55000, 60000, 65000, 70000, 75000, 80000, 85000, 90000, 95000, 100000,120000,130000]
    
    investor_names.each do |investor|
    balance = dummy_investor_amount_array.sample
    loan_category = ["All"]
    loan_category = ["Property"] if ["Bob"].include?(investor)
    loan_category = ["Property","Retail"] if ["Susan"].include?(investor)
    risk_band = (investor == "Jamie") ? ["C","D","E"] : ["All"] 
    risk_band = (investor == "George") ? ["A"] : risk_band

    invest_percentage = (investor == "Helen") ? 40 : 100
    investor_details[investor]= {balance: balance,remaining_balance: balance,invest_loan_category: loan_category,risk_band: risk_band,invest_percentage: invest_percentage}
    end

    loan_with_investor = []
    
    loan_records.each do |loan|
      is_get_loan = false
      loan_id = loan[:loan_id]
      loan_category = loan[:category]
      loan_risk_band = loan[:risk_band]
      loan_amount = loan[:amount]
      investor_details.each do |investor_key,investor_value|
        break if is_get_loan
        if investor_value[:invest_loan_category].include?("All") || investor_value[:invest_loan_category].include?(loan_category)
          if investor_value[:risk_band].include?("All") || investor_value[:risk_band].include?(loan_risk_band)
            investor_remaining_balance = investor_value[:remaining_balance]
            if (1..investor_remaining_balance).include?(loan_amount)
              investor_new_remaining_balance = (investor_remaining_balance - loan_amount).abs rescue 0
              investor_value[:remaining_balance] = investor_new_remaining_balance
              is_get_loan = true
              loan_with_investor << {loan_id: loan_id,loan_category: loan_category,risk_band: loan_risk_band,loan_amount: loan_amount,investor_name: investor_key,investor_balance: investor_value[:balance],investor_remaining_balance: investor_new_remaining_balance}
            end
          end
        end
      end
    end

    {"Total Loan" => loan_records.count,"Approved Loan" => loan_with_investor.count,"Approved Loan Details" => loan_with_investor,}
  end
end
